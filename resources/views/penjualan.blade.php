@extends('master')
@section('konten')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Ticket</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="card-header">
            <h3 class="card-title">Data Pelanggan</h3>
        </div>

        <div class="card-body">
            <form action="{{url('/main/jual')}}" method="post">
                    {{ csrf_field() }}
                <div class="form-group">
                    <label for="inputName">Nama Konsumen</label>
                    <input type="text" required="required" name="namakonsumen"  class="form-control">
                </div>
                <div class="form-group">
                    <label for="inputName">Alamat</label>
                    <input type="textarea" required="required" name="alamat"  class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="inputStatus">Stok</label>
                    <input type="number" required="required" name="stok" class="form-control">
                </div>
                <div>
                    <label for="inputName">Serial</label>
                        <select name="kodebarang[]" id="multiple" multiple>
                            @foreach($masterbarang as $row)
                            <option value="{{$row->kode_barang}}">{{$row->kode_barang}}-{{$row->namabarang}}</option>
                            @endforeach
                        </select>
                </div>

                <br>

                  <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-success">
                  </div>
                </div>
            </form>
        </div>

      </section>
      <!-- /.content -->



  @include('../layout.footer')

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('../layout.javascript')

</body>
</html>
@endsection
