@extends('master')
@section('konten')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Ticket</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="card-header">
            <h3 class="card-title">Data Pelanggan</h3>
        </div>

        <div class="card-body">
            <form action="{{url('main/update')}}" method="post">
                {{ csrf_field() }}
                <div><input name="id" type="hidden" value="{{ $masterbarang->id}}"></div>
                <div class="form-group">
                    <label for="inputName">Kode Barang</label>
                    <input type="text" value="{{ $masterbarang->kode_barang}}" required="required" name="kode_barang"  class="form-control">
                </div>
                <div class="form-group">
                    <label for="inputName">Nama Barang</label>
                    <input type="text" value="{{ $masterbarang->namabarang}}" required="required" name="namabarang"  class="form-control">
                </div>
                <div class="form-group">
                    <label for="inputStatus">Harga Jual</label>
                    <input type="number" value="{{ $masterbarang->hargajual}}" required="required" name="hargajual"  class="form-control">
                </div>
                <div class="form-group">
                    <label for="inputStatus">Harga Beli</label>
                    <input type="number" value=" {{ $masterbarang->hargabeli}}" required="required" name="hargabeli" class="form-control">
                </div>

                <div class="form-group">
                    <label for="inputStatus">Stok</label>
                    <input type="number" value="{{ $masterbarang->kode_barang}}" required="required" name="stok" class="form-control">
                </div>

                <div class="form-group">
                    <label for="inputDescription">Kategori</label>
                    <select class="form-control" name="kategori">
                      <option selected disabled>Select one</option>
                      <option value="hardware" id="">Hardware</option>
                      <option value="smartphone" id="">Smartphone</option>
                      <option value="laptop" id="">Laptop</option>
                      <option value="peripheral" id="">Peripheral</option>
                    </select>
                </div>

                <br>

                  <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-success">
                  </div>
                </div>
            </form>
        </div>

      </section>
      <!-- /.content -->



  @include('../layout.footer')

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('../layout.javascript')

</body>
</html>
@endsection
