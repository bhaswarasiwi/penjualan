
 @extends('master')
 @section('konten')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">

      <div class="row mb-2">
        <div class="col-sm-6">

          <h1 class="m-0 text-dark">Master Barang</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- /.row -->
      <!-- Main row -->


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <a href="{{ url('/main/tambah')}}" class="btn btn-primary" >tambah barang</a>
            </div>
            <div class="card-header">
              <h3 class="card-title"></h3>

            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 300px;">
                <table id="example" class="display" style="width:100%">
                    <thead align="center">
                    <tr class="header">

                    <th>Kode barang</th>
                    <th>Nama Barang</th>
                    <th>Harga Jual</th>
                    <th>Harga Beli</th>
                    <th>Stock</th>
                    <th>Kategori</th>
                    <th>Action</th>


                  </tr>
                </thead>
                <tbody>
                  @foreach($masterbarang as $p)
                  <tr class="item">
                    <td align="center">{{$p->kode_barang }}</td>
                    <td align="center">{{$p->namabarang }}</td>
                    <td align="center">{{$p->hargajual}}</td>
                    <td align="center">{{$p->hargabeli}}</td>
                    <td align="center">{{$p->stok}}</td>
                    <td align="center">{{$p->kategori}}</td>
                    <td align="center">
                        <a onclick="if(confirm('Do you want to delete ?')){}else{return false}" href="{{url('/')}}/main/delete/{{ encrypt($p->id) }} " class="btn btn-danger">Delete</a>
                        <a onclick="if(confirm('Do you want to edit ?')){}else{return false}" href="{{url('/')}}/main/edit/{{ encrypt($p->id) }} " class="btn btn-warning">Edit</a>
                    </td>
                  </tr>

                  @endforeach
                </tbody>
              </table>
            </div>

            <!-- footer -->



            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- modal-content -->







      </div>
      <!-- /.row -->
      <!-- /.content -->

      <!-- /.content-wrapper -->
      @include('../layout.footer')

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
  </div>
  <!-- ./wrapper -->


  <!-- jQuery -->
  @include('../layout.javascript')
  <!-- Bisa di tambahkan lagi jquery langsung disini jika di butuhkan -->
</body>
</html>
@endsection
