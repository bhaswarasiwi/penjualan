<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'mainController@index');
Route::post('/main/checklogin', 'mainController@checklogin');

Route::group(['middleware' => ['check_login']],function () {

Route::get('/', 'dashboard@index');
Route::get('/main/barang', 'barang@index');
Route::get('/main/tambah', 'barang@tambah');
Route::get('/main/edit/{id}', 'barang@edit');
Route::get('/main/delete/{id}', 'barang@hapus');

Route::post('/main/insert', 'barang@insert');
Route::post('/main/update', 'barang@update');

Route::get('/main/penjualan', 'penjualan@index');

Route::get('main/logout', 'mainController@logout');

});
