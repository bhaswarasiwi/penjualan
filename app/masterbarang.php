<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class masterbarang extends Model
{
    protected $table = "masterbarang";
    protected $guarded=["kode_barang"];
}
