<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
class check_login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $check = Session::get('userData')['email'];

        if($check == null){

            return redirect('/login');

        }

        return $next($request);
    }
}
