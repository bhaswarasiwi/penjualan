<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\masterbarang;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class barang extends Controller
{
    public function index(){
        $masterbarang = DB::table('masterbarang')->get();
        return view('barang',['masterbarang' => $masterbarang]);
    }

    public function tambah(){
        return view ('page.tambah');
    }

    public function edit($id){
        $id = Crypt::decrypt($id);
        $masterbarang = masterbarang::find($id);
         return view ('page.edit', ['masterbarang' => $masterbarang]);
    }

    public function hapus($id){
        $id = Crypt::decrypt($id);
        DB::delete('delete from masterbarang where kode_barang = ?',[$id]);
        return redirect(url('/main/barang'));
    }

    public function insert(Request $request){
        masterbarang::create([
        'kode_barang' => $request->kode_barang,
        'namabarang' => $request->namabarang,
        'hargajual' => $request->hargajual,
        'hargabeli' => $request->hargabeli,
        'stok' => $request->stok,
        'kategori' => $request->kategori,
    ]);
    return redirect(url('/main/barang'));
}

public function update(Request $request){
    DB::table('masterbarang')->where('id',$request->id)->update([
        'kode_barang' => $request->kode_barang,
        'namabarang' => $request->namabarang,
        'hargajual' => $request->hargajual,
        'hargabeli' => $request->hargabeli,
        'stok' => $request->stok,
        'kategori' => $request->kategori,
        ]);
        return redirect(url('/main/barang'));
}
}
