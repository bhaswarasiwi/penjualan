<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class mainController extends Controller
    {
        function index()
        {
         return view('login');
        }

        function checklogin(Request $request)
        {

         $this->validate($request, [
          'email'   => 'required|email',
          'password'  => 'required|alphaNum|min:3'
         ]);

         $user_data = array(
          'email'  => $request->get('email'),
          'password' => $request->get('password')
         );

         $check_data = DB::table('users')
         ->select('*')->where('email',$request->get('email'))
         ->where('password',$request->get('password'))
         -> first();

         if($check_data)
         {
            Session_start();
            Session::flush();
            Session::put(['userData' => [
                'email' => $request->get('email'),

            ]]);

          return redirect('/');
         }
         else
         {
          return back()->with('error', 'Wrong Login Details');
         }

        }


        function cekprogres()
        {

         return view('admin.cekprogres');
        }


        function logout()
        {
         Session_start();
         Session::flush();
         return redirect('/login');
        }
    }


?>
