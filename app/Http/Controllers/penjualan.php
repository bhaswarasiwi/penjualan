<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class penjualan extends Controller
{
    public function index(){
        $masterbarang = DB::table('masterbarang')->get();
        return view('penjualan',['masterbarang' => $masterbarang]);
    }
}
